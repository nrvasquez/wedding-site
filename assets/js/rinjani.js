$(function() {
    
    // jQuery for page loader
    $(window).load(function() {
        $('#rj-loader').fadeOut('fast');
    });
    
    
    // jQuery for the Side Menu
    $('#rj-sidebar').click(function(e) {
        e.preventDefault();
        $('#sidebar-wrapper').toggleClass('active').css('box-shadow', '0 0 13px rgba(0, 0, 0, 0.2)');
    });
    
    $('#menu-close').click(function(e) {
        e.preventDefault();
        $('#sidebar-wrapper').toggleClass('active').css('box-shadow', 'none');
    });
    
    
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.page-scroll a').bind('click', function(e) {
        e.preventDefault();
        
        var $anchor = $(this);
        
        var offset = $('body').attr('data-offset');
        
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - (offset - 1)
        }, 1500, 'easeInOutExpo');
    });
    
    
    // jQuery sticky
    $("#navigation").sticky({topSpacing: 0});
    
    // Contact Form Request
    $(".validate").validate();
    
    $(document).on('submit', '#contact-us-form', function(e) {
        e.preventDefault();
        
        $('.form-respond').html("<div class='content-message'><i class='fa fa-refresh fa-spin fa-4x'></i> <h2>Loading..</h2></div>");
        
        $.ajax({
            url: $('#contact-us-form').attr('action'),
            type: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                if (data == true) {
                    $('.form-respond').html("<div class='content-message'><i class='fa fa-rocket fa-4x'></i> <h2>Email Sent Successfully</h2> <p>Your message has been submitted.</p></div>");
                } else {
                    $('.form-respond').html("<div class='content-message'><i class='fa fa-exclamation-circle fa-4x'></i> <h2>Error sending</h2> <p>Try again later.</p></div>");
                }
            },
            error: function(xhr, err) {
                $('.form-respond').html("<div class='content-message'><i class='fa fa-exclamation-circle fa-4x'></i> <h2>Error sending</h2> <p>Try again later.</p></div>");
            }
        });
    }); 
});