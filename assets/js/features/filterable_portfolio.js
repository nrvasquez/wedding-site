$(window).load(function(){ 
    var $container = $('.popup-portfolio .row'); 
    
    $container.isotope({ 
        filter: '*', 
        animationOptions: { 
            duration: 750, 
            easing: 'linear', 
            queue: false
        } 
    }); 
    
    $('.portfolio-ul-wrapper ul a').click(function(){ 
        var selector = $(this).attr('data-filter'); 
        
        $container.isotope({ 
            filter: selector, 
            animationOptions: { 
                duration: 750, 
                easing: 'linear',
                queue: false
            }
        });
        
        return false;
    });
    
    var $optionSets = $('.portfolio-ul-wrapper ul'), 
        $optionLinks = $optionSets.find('a'); 
   
    $optionLinks.click(function(){ 
        var $this = $(this); 
        
        // don't proceed if already selected 
        if ( $this.hasClass('selected')) { 
            return false; 
        } 
        
        var $optionSet = $this.parents('.portfolio-ul-wrapper ul'); 
        $optionSet.find('.selected').removeClass('selected'); 
        $this.addClass('selected');  
    });
});